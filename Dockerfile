FROM openjdk:17.0.2-oraclelinux8
VOLUME /tmp
COPY "./target/ActividadDocker-0.0.1-SNAPSHOT.jar" "Container-Actividad.jar"
EXPOSE 8080
ENTRYPOINT ["java","-jar","Container-Actividad.jar"]