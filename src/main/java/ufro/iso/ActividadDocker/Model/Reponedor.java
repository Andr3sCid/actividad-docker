package ufro.iso.ActividadDocker.Model;

public class Reponedor extends Usuario {
    private String sección;

    public Reponedor(String nomrbe, String rut, String usuario, String contraseña, String seccion) {
        super(nomrbe, rut, usuario, contraseña);
        this.sección = seccion;
    }

    public String getSección() {
        return sección;
    }

    public void setSección(String sección) {
        this.sección = sección;
    }
}
