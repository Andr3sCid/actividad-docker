package ufro.iso.ActividadDocker.Model;

public class Cajero extends Usuario{
    private int ventas;

    public Cajero(String nomrbe, String rut, String usuario, String contrasena, int ventas) {
        super(nomrbe, rut, usuario, contrasena);
        this.ventas = ventas;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
