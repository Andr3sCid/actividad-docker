package ufro.iso.ActividadDocker.Model;

public class Usuario {
    private String nombre;
    private String rut;
    private String usuario;
    private String contrasena;

    public Usuario(String nomrbe, String rut, String usuario, String contrasena) {
        this.nombre = nomrbe;
        this.rut = rut;
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
