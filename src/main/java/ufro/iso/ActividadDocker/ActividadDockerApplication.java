package ufro.iso.ActividadDocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActividadDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActividadDockerApplication.class, args);
	}

}
