package ufro.iso.ActividadDocker.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ufro.iso.ActividadDocker.Model.Cajero;
import ufro.iso.ActividadDocker.Model.Merma;
import ufro.iso.ActividadDocker.Model.Producto;
import ufro.iso.ActividadDocker.Model.Reponedor;

import java.util.List;

@RestController
@RequestMapping("/Visualizar")
public class VisualizarController {
    @GetMapping("/Productos")
    public List<Producto> verProductos(){
        return List.of(new Producto[]{new Producto("Arroz", "Cereales", 1000, "acá"),
                       new Producto("Lenteja", "Legunbres", 9, "allá"),
                       new Producto("Pan", "Panaderia", 10, "ahí"),
                       new Producto("Cecina", "Cecinaría", 20, "aquí"),
                       new Producto("Queso", "Quesería", 20, "Por allá"),
                       new Producto("Lomo vetado", "carnicaría", 9000, "allí"),
                       new Producto("Fideos", "Pastas", 900, "de este lado"),
                       new Producto("Leche semidescremada colún", "Lateos", 900, "a la vuelta")});
    }

    @GetMapping("/Reponedor")
    public List<Reponedor> verReponedores(){
        return List.of(
                new Reponedor("Ariel", "1234567-8", "Detergente", "12345", "Cereales"),
                new Reponedor("Rapuncel", "124354658-7", "Trencitas", "4321", "Cecinería"),
                new Reponedor("Tiana", "4356021-5", "anura", "42536", "Quesería")
        );
    }

    @GetMapping("/Cajeros")
    public List<Cajero> verCajeros(){
        return List.of(
                new Cajero("Pedro", "102485382-4", "Ju4n^D13g0", "192837", 5),
                new Cajero("Sanders","5968395-6", "KFC-Manager", "pollo con papas", 15),
                new Cajero("Fred DeLuca", "6346563-6", "Subwayreano", "D3L_d14", 566),
                new Cajero("Oscar", "375726483-4", "Doggos", "c0mpl3t0",6),
                new Cajero("Ray", "4654825-6", "McTripleSinpAN", "Bac0n", 12)
        );
    }

    @GetMapping("/Mermas")
    public List<Merma> verMermas(){
        return List.of(
                new Merma("Reduccion de limpiadores Baños", "Pocos guardias", 6),
                new Merma("Aumento de paquetes de fideos de porotos Trendy", "Nueva distribución de pastas", 9999),
                new Merma("Disminución de hinodoros invertidos", "pocas ventas", 5)
        );
    }
}
